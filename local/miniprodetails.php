<?PHP
		session_start();
		include("header.php"); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Search Results</title>
  
<link rel="stylesheet" href="form.css">
<link rel="stylesheet" href="tables.css">
<link rel="stylesheet" href="mpd.css">

<script>
$(window).scroll(function(e){ 
  var $el = $('.subnav'); 
  var isPositionFixed = ($el.css('position') == 'fixed');
  if ($(this).scrollTop() > 200 && !isPositionFixed){ 
    $('.subnav').css({'position': 'fixed', 'top': '-50px','left':'0%'}); 
  }
  if ($(this).scrollTop() < 200 && isPositionFixed)
  {
    $('.subnav').css({'position': 'static', 'top': '0px','margin-left':'0px'}); 
  } 
});

</script>
</head>


<body id="myPage" data-spy="scroll" data-target=".subnav" data-offset="60">
  
 <div class="container">
	<div class="subnav ">	
	
	<ul class="nav nav-tabs">
		<li><a href="#project">Project Details</a></li>
		<li><a href="#student">Team and Students Details</a></li>
		<li><a href="#college">College Details</a></li>
		<li><a href="#guide">Guide Details</a></li>
		<li><a href="#files">Video and Source Files</a></li>
		<li><a href="#techs">Technologies Used</a></li>
	</ul>
	</div>
		
	</div> 

<?php

include("dbconfig.php");
$page=null;
  if(isset($_GET["id"])) {
$ele=$_GET["id"];
  }
     $_SESSION['search_text']= $ele;
	 if($ele == null)
		   {
		   		echo "<script> alert('please enter text')
				history.go(-1);</script>";
				//header('Location:home.php');
		   }
	else{
?>

<!-- Container (project Section) -->
<div id="project" class="container-fluid bg-grey">	

  <div class="row">
    <div class="col-sm-8">
<h2>Project Details:</h2>
<div class="container">
 <div class="sgntb">	
<?php		
	
	$query="select m.*,t.*,c.* from miniproject m,team t,colleges c where m.pnm='$ele' and m.tid=t.tid and c.cid=m.cid ";
		$results = mysqli_query($conn,$query);
		echo "<table><br/>";
		if ($results->num_rows > 0) {
			while ($row = mysqli_fetch_array($results)) {
			
				
				 $pid = $row['pid'];
				// echo "<h4>".$row['pnm']."</h4>";
				 echo "<table id='colleges'>";
				 echo "<tr><th>Name</th>";
				 echo "<td>".$row['pnm']."</td></tr>";
				 
				 echo "<tr><th>Category 1</th>";
				 echo "<td>".$row['pcat1']."</td></tr>";
				 
				 echo "<tr><th>Category 2</th>";
				 echo "<td>".$row['pcat2']."</td></tr>";
				 
				 echo "<tr><th>Duration in Months</th>";
				 echo "<td>".$row['pdura']."</td></tr>";
				 	 
				 echo "<tr><th>Project Status</th>";
				 echo "<td>".$row['pstatus']."</td></tr>";
				 
				 echo "<tr><th>Project Stage</th>";
				 echo "<td>".$row['pstg']."</td></tr>";
				
				 echo "<tr><th>Problem Statement</th>";
				 echo "<td>".$row['prbstmt']."</td></tr>";
				
				 echo "<tr><th>Problem Description</th>";
				 echo "<td>".$row['prbdesc']."</td></tr>";
				 
				 echo "<tr><th>Already Exists</th>";
				 echo "<td>".$row['ae']."</td></tr>";
				 
				 echo "<tr><th>Advantages</th>";
				 echo "<td>".$row['advs']."</td></tr>";
				 echo "</table>";?><br/><?php
			}
		}
?>

</div>
</div>
</div>
</div>

</div>

<!-- Container (student Section) -->
<div id="student" class="container-fluid">	

  <div class="row">
    <div class="col-sm-8">
<h2>Team and Students Details:</h2>
<div class="container">
 <div class="sgntb">	
<?php			

$query="select m.*,t.*,c.* from miniproject m,team t,colleges c where m.pnm='$ele' and m.tid=t.tid and c.cid=m.cid ";
		$results = mysqli_query($conn,$query);
		echo "<table><br/>";
		if ($results->num_rows > 0) {
			while ($row = mysqli_fetch_array($results)) {
					
				$tid=$row['tid'];
				echo "<table id='colleges'>";	
				echo "<th>Team Name</th>";
				 echo "<td>". $row['tnm']."</td></tr>";
				 
				  echo "<th>Team Status</th>";
				 echo "<td>". $row['tstatus']."</td></tr>";
				 
				 echo "<th>Date Created</th>";
				 echo "<td>". $row['tdoc']."</td></tr>";	
				 echo "</table>";	

			}
		}
		
		$query1="select * from teamstuds where tid='$tid'";
		$results1 = mysqli_query($conn,$query1);
			echo "<table><br/>";
				if ($results1->num_rows > 0) {
			while ($row1 = mysqli_fetch_array($results1)) { 
			
			$sid=$row1['sid'];
			
			$query2="select * from students where sid='$sid'";
		$results2 = mysqli_query($conn,$query2);
			echo "<table><br/>";
				if ($results2->num_rows > 0) {
			while ($row2 = mysqli_fetch_array($results2)) {
			
				 echo "<table id='colleges'>";	
				echo "<th>Name</th>";
				 echo "<td>". $row2['sfnm']." ".$row2['smnm']." ".$row2['slnm']."</td></tr>";
				 
				  echo "<th>Course</th>";
				 echo "<td>". $row2['course']."</td></tr>";
				 
				 echo "<th>Branch</th>";
				 echo "<td>". $row2['brnch']."</td></tr>";
				 
				  echo "<th>Email</th>";
				 echo "<td>". $row2['sem']."</td></tr>";
				 
				  echo "<th>Phone</th>";
				 echo "<td>". $row2['sphn']."</td></tr>";
				 
				  echo "<th>Active Status</th>";
				 echo "<td>". $row2['sstat']."</td></tr>";
				
				 echo "</table>";
				?><br/><?php
			}
		}
			}
				}
				 
?>
</div>
</div>
</div>
</div>
</div>

		
<!-- Container (college Section) -->
<div id="college" class="container-fluid bg-grey">	

  <div class="row">
    <div class="col-sm-8">
<h2>College Details:</h2>
<div class="container">
 <div class="sgntb">	
<?php	

$query="select m.*,t.*,c.* from miniproject m,team t,colleges c where m.pnm='$ele' and m.tid=t.tid and c.cid=m.cid ";
		$results = mysqli_query($conn,$query);
		echo "<table><br/>";
		if ($results->num_rows > 0) {
			while ($row = mysqli_fetch_array($results)) {
						 
				echo "<table id='colleges'>";	
			
				 echo "<tr><th>College Name</th>";
				 echo "<td>". $row['cname']."</td></tr>";
				 
				 
				 echo "<tr><th>University</th>";
				 echo "<td>". $row['univ']."</td></tr>";
				 
				 
				 echo "<tr><th>Email</th>";
				 echo "<td>". $row['cem']."</td></tr>";
				 
				 echo "<tr><th>Phone</th>";
				 echo "<td>". $row['cphn']."</td></tr>";
				 
				 echo "<tr><th>Address</th>";
				 echo "<td>". $row['cadd'].", ".$row['ccity'].", ".$row['cstate']."</td></tr>";
				echo "</table>";?><br/><?php			 
			}
		}			
?>
</div>	
</div>	
</div>	
</div>	
</div>	

	
<!-- Container (college Section) -->
<div id="guide" class="container-fluid bg-grey">	

  <div class="row">
    <div class="col-sm-8">
<h2>Guide Details:</h2>
<div class="container">
 <div class="sgntb">	
<?php	

$query="select m.*,t.*,c.*,g.* from miniproject m,team t,colleges c,guide g
        where m.pnm='$ele' and m.tid=t.tid and c.cid=m.cid and m.gid=g.gid";
		$results = mysqli_query($conn,$query);
		echo "<table><br/>";
		if ($results->num_rows > 0) {
			while ($row = mysqli_fetch_array($results)) {
						 
				echo "<table id='colleges'>";	
			
				 echo "<tr><th>Name</th>";
				 echo "<td>". $row['gnm']."</td></tr>";
				 
				 
				 echo "<tr><th>Designation</th>";
				 echo "<td>". $row['gdesg']."</td></tr>";
				 
				 
				 echo "<tr><th>Email</th>";
				 echo "<td>". $row['gem']."</td></tr>";
				 
				 echo "<tr><th>Phone</th>";
				 echo "<td>". $row['gphn']."</td></tr>";
				 
				  echo "<tr><th>Department</th>";
				 echo "<td>". $row['gdpt']."</td></tr>"; 
				 
				  echo "<tr><th>Experience</th>";
				 echo "<td>". $row['expyr']." Years</td></tr>";
				  
				  echo "<tr><th>Specification</th>";
				 echo "<td>". $row['spec']."</td></tr>";
				 
				echo "</table>";?><br/><?php			 
			}
		}			
?>
</div>	
</div>	
</div>	
</div>	
</div>	

		
<!-- Container (files Section) -->
<div id="files" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
<h2>Video and Source File:</h2>
<div class="container">
 <div class="sgntb">	
<?php				


$query="select m.*,t.tnm,c.cname from miniproject m,team t,colleges c where
        m.pnm='$ele' and m.tid=t.tid and c.cid=m.cid ";
		$results = mysqli_query($conn,$query);
		
		if ($results->num_rows > 0) {
			while ($row = mysqli_fetch_array($results)) {
				
echo "<h2>Video:</h2>" ;
 if($row['pvid']!=null){
 echo'<iframe width="560" height="315" src="'.$row['pvid'].'" frameborder="0" 
allowfullscreen></iframe>'; }
else{
	echo '<h4>VIDEO LINK NOT AVIALABLE</h4>';
}
		
?>
<br/><br/>
<?php
				
echo "<h2>Source File:</h2>" ;  
 if($row['sf']!=null){
	?><h4><a href= "<?php echo $row['sf'];?>" target="_blank">Github - Project Link</a></h4><?php
	}
else{
	echo '<h4>GITHUB LINK NOT AVIALABLE</h4>';
}
	}
		}?>
</div>
</div>
</div>
</div>
</div>

<!-- Container (Techs Section) -->
<div id="techs" class="container-fluid bg-grey">	
<div class="row">
    <div class="col-sm-8">
<h2>Technologies Used:</h2>
<div class="container">
 <div class="sgntb">

<?php

				
		$query="select m.*,t.*,c.* from miniproject m,team t,colleges c where
		m.pnm='$ele' and m.tid=t.tid and c.cid=m.cid ";
		$results = mysqli_query($conn,$query);
		echo "<table><br/>";
		if ($results->num_rows > 0) {
			while ($row = mysqli_fetch_array($results)) {
				 
				$q3="select * from techused where pid='$pid'";
				$result3 = $conn->query($q3);
	
	
				if ($result3->num_rows > 0) {
				echo "<table id='colleges'><tr>
				<th>Technology</th>
				<th>Category</th>
				</tr>";
				// output data of each row
				while($row3 = $result3->fetch_assoc()) {
					$techid=$row3["techid"];
					$q4="select * from technologies where techid='$techid'";
					$result4 = $conn->query($q4);
					$row4 = $result4->fetch_assoc();
		
					echo '<tr><td>' .$row4["technm"]. '</td><td>' .$row4["techcat"]. '</td></tr>';
				}
				echo "</table>";?><br/><?php
				}
	
				else {
					echo "0 results";
				}
			}
			
		}
	}


?>
</div>
</div>
</div>
</div>
  </div>



</body>
</html>
<?php include("footer.html"); ?>