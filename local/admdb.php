<?php

		session_start();
		include("header.php"); 

  //echo $_SESSION['login_user'];
  if($_SESSION['login_user'] != 'Admin'){
	if($_SESSION['login_user'] == 'College Representative'){
	  header('location:collrep.php');
	}
    else if($_SESSION['login_user'] == 'Student'){
	  header('location:stud.php');
	}
    else if($_SESSION['login_user'] == 'College'){
	  header('location:college.php');
	}
	else{
  	  header('location:signinform.php');
	}
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Admin</title>
   <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link href="jquery.paginate.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="body.css">
<link rel="stylesheet" href="form.css">
<link rel="stylesheet" href="tables.css">
 
  		<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="jquery.easyPaginate.js"></script>
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">



<!-- Container (dashboard Section) -->
<!-- Existing colleges -->


<div id="db" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
	
	<br/><h2>DASHBOARD</h2>
     <button><a href="changepswform.php" align="right">Change Password</a></button>
	
   
<?php
include("dbconfig.php");

$sql = "select cid,cname,univ,cadd,ccity,cstate,cem,cphn from colleges where isDeleted=0";
$result = $conn->query($sql);?>
<div class="container">
 <div class="sgntb">
<?php echo '<form method="post" action="deletecoll.php">';?>
<h4><b>Existing Colleges</b></h4>
<?php
if ($result->num_rows > 0) {
    echo "<table id='colleges'><thead><tr>
	
	<th>Name</th>
	<th>University</th>
	<th>Address</th>
	<th>City</th>
	<th>State</th>
	<th>Mail</th>
	<th>Phone</th>
    <th>"; ?> <input type='submit' name='save' value='DELETE'> <?php echo "</th>
	</tr></thead><tbody>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
		$cid=$row["cid"];
		$cname=$row["cname"];
		 echo "<tr><td><a href='colldetails.php?id=$cid'>". $cname."</a></td>";
        echo '<td>' .$row["univ"]. '</td>
				<td>' .$row["cadd"]. '</td><td>' .$row["ccity"]. '</td><td>' .$row["cstate"]. '</td>
				<td>' .$row["cem"]. '</td><td>' .$row["cphn"]. '</td>
				<td><input type="checkbox" id="checkItem" name="check[]"  value='.$cid.'></td></tr>';
    }
	
    echo "</tbody>";
    echo "</table>";
} else {
    echo "0 results";
}
echo '</form>';
echo '<script>
$("#checkAl").click(function () {
$("input:checkbox").not(this).prop("checked", this.checked);
});
</script>';

?>

	  
    </div>
  
  </div>


<!-- Pending colleges Container (dashboard Section) -->

<?php
$sql = "select * from colleges where IsDeleted='2'";
$result = $conn->query($sql);?>
<div class="container">
 <div class="sgntb">
 <?php
echo '<form method="post" action="aprj.php">';?>
<h4><b>Pending Approval Colleges</b></h4>
<?php
if ($result->num_rows > 0) {
    echo "<table id='colleges'><thead><tr>
	
	<th>Name</th>
	<th>University</th>
	<th>Address</th>
	<th>City</th>
	<th>State</th>
	<th>Mail</th>
	<th>Phone</th>
	<th>"; ?> <input type='submit' name='savea' value='APPROVE'> <?php echo "</th>
	<th>"; ?> <input type='submit' name='saver' value='REJECT'> <?php echo "</th>
	</tr></thead><tbody>";
    // output data of each row
	
    while($row = $result->fetch_assoc()) {
		$cid=$row["cid"];$cname=$row["cname"];
		 echo "<tr><td><a href='colldetails.php?id=$cid'>". $cname."</a></td>";
        echo '<td>' .$row["univ"]. '</td>
				<td>' .$row["cadd"]. '</td><td>' .$row["ccity"]. '</td><td>' .$row["cstate"]. '</td>
				<td>' .$row["cem"]. '</td><td>' .$row["cphn"]. '</td>
				<td><input type="checkbox" id="checkItema" name="checka[]" value='.$cid.'></td>
				<td><input type="checkbox" id="checkItemr" name="checkr[]" value='.$cid.'></td></tr>';
    }
	
    echo "</tbody>";
    echo "</table>";
	echo "<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
		<script src='jquery.paginate.js'></script>
		<script>
		$(document).ready(function () {
		$('table').paginate({
        'elemsPerPage': 2,
            'maxButtons': 6
    });
});
</script>";

	
} else {
    echo "0 results";
}
echo '</form>';
echo '<script>
$("#checAl").click(function)() {
	$("input:checkbox").not(this).prop("checked",this.checked);
});
</script>';

$conn->close();
?>   

	    
    </div>
  
  </div>
    </div>
  
  </div>
</div>



<!-- Container (adm add Section) -->

<div id="add" class="container-fluid bg-grey">
  <div class="row">
    <div class="col-sm-8">
      <h2>ADD COLLEGE DETAILS</h2><br>
	<div class="container">
 <div>
 
 
<div class="container">
 <div class="sgn">
 <h4><b>BULK UPLOAD</b></h4>

 <form action="addcollb.php" method="post" enctype="multipart/form-data"> 
 
    Select file to upload: 
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	
	<a href="sample_college_details_file.csv"><input type="button" value="Download Sample File"></a>

    <input type="file" name="collfile" id="collfile"> <br/>
    <input type="submit" value="Upload File" name="submitb" id="submitb"><br/><br/>
</form>

</div>
</div>

<div class="container">
 <div class="sgn">
 <h4><b>MANUAL UPLOAD</b></h4>
  <form action="addcoll.php" method="post">
  
  <h4><b>College Details</b></h4>
    <span>Name:</span>
	<input type="text" class="inp" id="cname" name="cname" placeholder="Name">

    <span>University: </span>
	<input type="text" class="inp" id="univ" name="univ" placeholder="University"> 

	<span >Autonomous: </span>
	<input type="radio" id="atnms" name="atnms" value="Yes">Yes
	<input type="radio" id="atnms" name="atnms" value="No">No <br/><br/>
		
    <span>Address: </span>
	<input type="text" class="inp" id="cadd" name="cadd" placeholder="Address"> 
    <span>City: </span>
	<input type="text" class="inp" id="ccity" name="ccity" placeholder="City"> 
    <span>State: </span>
	<input type="text" class="inp" id="cstate" name="cstate" placeholder="State"> 
	<span>Email:</span>
	<input type="email" class="inp" id="cem" name="cem" placeholder="Email"> 
	<span>Phone:</span>
	<input type="number" class="inp" id="cphn" name="cphn" placeholder="Phone"> <br/>
	
	<h4><b>Representative 1:</b></h4>
	<span>Name:</span>
	<input type="text" class="inp" id="cr1nm" name="cr1nm" placeholder="Name"> 
	<span>Email:</span>
	<input type="email" class="inp" id="cr1em" name="cr1em" placeholder="Email"> 
	<span>Phone:</span>
	<input type="number" class="inp" id="cr1phn" name="cr1phn" placeholder="Phone"> <br/>
	
	<h4><b>Representative 2:</b></h4>
	<span>Name:</span>
	<input type="text" class="inp" id="cr2nm" name="cr2nm" placeholder="Name"> 
	<span>Email:</span>
	<input type="email" class="inp" id="cr2em" name="cr2em" placeholder="Email"> 
	<span>Phone:</span>
	<input type="number" class="inp" id="cr2phn" name="cr2phn" placeholder="Phone"> <br/><br/>
		
	<input type="submit" value="Submit">
  </form>
	 
</div>

</div>
</div>

</div>
      
    </div>
  
  </div>
</div>


</body>
</html>
<?php include("footer.html"); ?>