<?php
		session_start();
		include("header.php"); 
  //echo $_SESSION['login_user'];
  //echo $_SESSION['login_user_name'];
  if($_SESSION['login_user'] != 'Student'){
	  session_unset();
	  session_destroy();
  	  header('location:signinform.php');
	}
  
  

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Personalize_Student</title>
<link rel="stylesheet" href="body.css">
<link rel="stylesheet" href="form.css">
<link rel="stylesheet" href="tables.css">
  
  
  
</head>
<body>
<br/><br/>
<div class="container">
<h4><u><b><p align="center"><a href="stud.php" >Back</a></u></b></h4></p>
 <div class="sgn">
 
  
   <form action="prsnlvld.php" method="post">
  
  
	<h4><b>Student Details</b></h4>
  
	<span>Expected Year of Completion: </span>
	<input type="number" class="inp" id="scmpl" name="scmpl" placeholder="YYYY"><br/><br/>
	

	
	 <span>Active Status: </span>
	<input type="text" class="inp" id="sstat" name="sstat" placeholder="Yes/No"><br/><br/>
	

	<span>Phone:</span>
	<input type="number" class="inp" id="sphn" name="sphn" placeholder="Phone"> <br/><br/>
	

	<span>Address:</span>
	<input type="text" class="inp" id="sadd" name="sadd" placeholder="Address"> 
	<span>City:</span>
	<input type="text" class="inp" id="scity" name="scity" placeholder="City"> 
	<span>State:</span>
	<input type="text" class="inp" id="sstate" name="sstate" placeholder="State"> <br/><br/><br/>
	
	
	
	
	<h4><b>Technology Known</b></h4>
   
   <span><h4>Languages:</h4></span>
	<input type="text" class="inp" id="lang" placeholder="Enter Language"/>
	Active Status:<select class="inp" id="lang2">
	<option value="Yes">Yes</option>
	<option value="No">No</option>
	</select>
	Expert Level:<select class="inp" id="lang3">
	<option value="Beginner">Beginner</option>
	<option value="Intermediate">Intermediate</option>
	<option value="Advanced">Advanced</option>
	</select>
	
	<div id="div_quotes1"> </div><br/>
	<input type="button" value="Add" onclick="addlang()"><br/><br/>
	
	
	
	<span><h4>IDEs:</h4></span>
	<input type="text" class="inp" id="ide" placeholder="Enter IDE">
	Active Status:<select class="inp" id="ide2">
	<option value="Yes">Yes</option>
	<option value="No">No</option>
	</select>
	Expert Level:<select class="inp" id="ide3">
	<option value="Beginner">Beginner</option>
	<option value="Intermediate">Intermediate</option>
	<option value="Advanced">Advanced</option>
	</select>
	<div id="div_quotes2"> </div><br/>
	<input type="button" value="Add" onclick="addide()"><br/><br/>
	
	 
	 
	 
	<span><h4>Web Technologies:</h4></span>
	<input type="text" class="inp" id="wt" placeholder="Enter Web Technology">
	Active Status:<select class="inp" id="wt2">
	<option value="Yes">Yes</option>
	<option value="No">No</option>
	</select>
	Expert Level:<select class="inp" id="wt3">
	<option value="Beginner"     >Beginner</option>
	<option value="Intermediate"  >Intermediate</option>
	<option value="Advanced"  >Advanced</option>
	</select>
	<div id="div_quotes3"> </div><br/>
	<input type="button" value="Add" onclick="addwt()"><br/><br/>
	
	
	
	<span><h4>Servers:</h4></span>
	<input type="text" class="inp" id="serv" placeholder="Enter Server">
	Active Status:<select class="inp" id="serv2">
	<option value="Yes">Yes</option>
	<option value="No">No</option>
	</select>
	Expert Level:<select class="inp" id="serv3"   >
	<option value="Beginner"  >Beginner</option>
	<option value="Intermediate" >Intermediate</option>
	<option value="Advanced" >Advanced</option>
	</select>
	<div id="div_quotes4"> </div><br/>
	<input type="button" value="Add" onclick="addserv()"><br/><br/>
	
	
	 
	<span><h4>Databases:</h4></span>
	<input type="text" class="inp" id="dtb" placeholder="Enter Database">
	Active Status:<select class="inp" id="dtb2"  >
	<option value="Yes" >Yes</option>
	<option value="No">No</option>
	</select>
	Expert Level:<select class="inp" id="dtb3"  >
	<option value="Beginner">Beginner</option>
	<option value="Intermediate" >Intermediate</option>
	<option value="Advanced" >Advanced</option>
	</select>
	<div id="div_quotes5"> </div><br/>
	<input type="button" value="Add" onclick="adddtb()"><br/><br/>
	
	
	
	<span><h4>Others:</h4></span>
	<input type="text" class="inp" id="other" placeholder="Enter">
	Active Status:<select class="inp" id="other2">
	<option value="Yes"    >Yes</option>
	<option value="No" >No</option>
	</select>
	Expert Level:<select class="inp" id="other3">
	<option value="Beginner"  >Beginner</option>
	<option value="Intermediate">Intermediate</option>
	<option value="Advanced">Advanced</option>
	</select>
	<div id="div_quotes6"> </div><br/>
	<input type="button" value="Add" onclick="addother()"><br/><br/>
	
	
	<input type="submit" value="Submit" name="submitp" id="submitp">
	
  </form>
  
</div>

</div>

<script>
function addlang(){
			var k=document.getElementById("lang").value;
			var k2=document.getElementById("lang2").value;
			var k3=document.getElementById("lang3").value;
			
			//document.getElementById("lang").value = " ";
            var div = document.getElementById('div_quotes1');
            div.innerHTML += "Language: <input name='lang[]' value="+k+" readonly></input>";
            div.innerHTML += "Active Status:<input name='lang2[]' value="+k2+" readonly></input>";
            div.innerHTML += "Expert Level:<input name='lang3[]' value="+k3+" readonly></input><br/>";
            div.innerHTML += "\n<br/>";
        }

function addide(){
    var k=document.getElementById("ide").value;
    var k2=document.getElementById("ide2").value;
    var k3=document.getElementById("ide3").value;
	
		//	document.getElementById("ide").value = " ";
            var div = document.getElementById('div_quotes2');
            div.innerHTML += "IDE:<input   name='ide[]' value="+k+" readonly></input>";
			div.innerHTML += "Active Status:<input   name='ide2[]' value="+k2+" readonly></input>";
			div.innerHTML += "Expert Level:<input   name='ide3[]' value="+k3+" readonly></input><br/>";
            div.innerHTML += "\n<br />";
}

function addwt(){
    var k=document.getElementById("wt").value;
    var k2=document.getElementById("wt2").value;
    var k3=document.getElementById("wt3").value;
		//	document.getElementById("wt").value = " ";
            var div = document.getElementById('div_quotes3');
            div.innerHTML += "Web Technology:<input   name='wt[]' value="+k+" readonly></input>";
            div.innerHTML += "Active Status:<input   name='wt2[]' value="+k2+" readonly></input>";
            div.innerHTML += "Expert Level:<input   name='wt3[]' value="+k3+" readonly></input><br/>";
            div.innerHTML += "\n<br />";
}

function addserv(){
   var k=document.getElementById("serv").value;
   var k2=document.getElementById("serv2").value;
   var k3=document.getElementById("serv3").value;
		//	document.getElementById("serv").value = " ";
            var div = document.getElementById('div_quotes4');
            div.innerHTML += "Server:<input   name='serv[]' value="+k+" readonly></input>";
            div.innerHTML += "Active Status:<input   name='serv2[]' value="+k2+" readonly></input>";
            div.innerHTML += "Expert Level:<input   name='serv3[]' value="+k3+" readonly></input><br/>";
            div.innerHTML += "\n<br />";
}  

function adddtb(){
   var k=document.getElementById("dtb").value;
   var k2=document.getElementById("dtb2").value;
   var k3=document.getElementById("dtb3").value;
		//	document.getElementById("dtb").value = " ";
            var div = document.getElementById('div_quotes5');
            div.innerHTML += "Database:<input   name='dtb[]' value="+k+" readonly></input>";
            div.innerHTML += "Active Status:<input   name='dtb2[]' value="+k2+" readonly></input>";
            div.innerHTML += "Expert Level:<input   name='dtb3[]' value="+k3+" readonly></input><br/>";
            div.innerHTML += "\n<br />";
}  

function addother(){
   var k=document.getElementById("other").value;
   var k2=document.getElementById("other2").value;
   var k3=document.getElementById("other3").value;
		//	document.getElementById("other").value = " ";
            var div = document.getElementById('div_quotes6');
            div.innerHTML += "Others:<input name='other[]' value="+k+" readonly></input>";
            div.innerHTML += "Active Status:<input name='other2[]' value="+k2+" readonly></input>";
            div.innerHTML += "Expert Level:<input name='other3[]' value="+k3+" readonly></input><br/>";
            div.innerHTML += "\n<br/>";
}
 



</script>


</body>
</html>
<?php include("footer.html"); ?>