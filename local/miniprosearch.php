<?php
		
		header('Cache-Control: no cache'); //no cache
		session_cache_limiter('private_no_expire'); // works
		//session_cache_limiter('public'); // works too
		session_start();
		include("header.php"); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Search</title>
  
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
   <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link href="jquery.paginate.css" rel="stylesheet" type="text/css">
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">



<!-- Container (Search Section) -->
<div id="search" class="container-fluid bg-grey">
  <div class="row">
    <div class="col-sm-8">
      
	  
<?php



include("dbconfig.php");
if(isset($_POST["search"])){
	  $ele=$_POST["searchele"];
	  
	    
	 if($ele == null)
		   {
		   	echo "<script> alert('Please Enter Text');
			history.go(-1);</script>";
			//header('Location:home.php');
	   
		   }
	else{
		
	?> <h2>Search Results</h2><br> <?php
		// based on Project name
		
		$query1="select * from miniproject where pnm like '%$ele%'";
		
		$results1 = mysqli_query($conn,$query1);
		
		
		if ($results1->num_rows > 0) {
			?><div id="search"><?php
			$i=1;
			while ($row1 = mysqli_fetch_array($results1)) {
				$pnm=$row1["pnm"];
				?>
				
  <div class="w3-card-4" style="width:50%;">
    <header class="w3-container" style="background-color:#435e76"  >
      <h2 style="color:white" ><?php echo $pnm; ?></h2>
    </header>

    <div class="w3-container">
      <br/><p><?php echo $row1["prbdesc"]; ?></p>
    </div>

    <footer class="w3-container w3-center">
      <h4> <?php echo "<button class='btn btn-primary'><a href='miniprodetails.php?id=$pnm'>View</a></button>";?></h4>
    </footer>
  </div><br/>
  <?php
			}
			?></div><?php
		
			
		}
		
		
		// based on Technology used
		
		$sql1 = "select * from technologies where technm like '%$ele%'";
		$result1 = $conn->query($sql1);
		if ($result1->num_rows > 0) {
			?><div id="search"><?php
		while($row1 = $result1->fetch_assoc()){
			
			$techid=$row1['techid'];

			$sql2 = "select * from techused where techid='$techid'";
			$result2 = $conn->query($sql2);
			if ($result2->num_rows > 0) {
			while($row2 = $result2->fetch_assoc()){
					
				$pid=$row2['pid'];
				$sql3 = "select * from miniproject where pid='$pid'";
				$result3 = $conn->query($sql3);
				if($result3->num_rows > 0) {
				$i=1;
				while($row3 = $result3->fetch_assoc()){
					$pnm=$row3["pnm"];
					?>	
  <div class="w3-card-4" style="width:50%;">
    <header class="w3-container" style="background-color:#435e76"  >
      <h2 style="color:white" ><?php echo $pnm; ?></h2>
    </header>

    <div class="w3-container">
      <br/><p><?php echo $row3["prbdesc"]; ?></p>
    </div>

    <footer class="w3-container w3-center">
      <h4> <?php echo "<button class='btn btn-primary'><a href='miniprodetails.php?id=$pnm'>View</a></button>";?></h4>
    </footer>
  </div><br/><?php
				}
				}
			}
			}
		}
		?></div><?php
	
		}
		
		
		//based on colleges

		$sql1 = "select * from colleges where cname like '%$ele%'";
		$result1 = $conn->query($sql1);
		if ($result1->num_rows > 0) {
			?><div id="search"><?php
		while($row1 = $result1->fetch_assoc()){
			
			$cid=$row1['cid'];

			$sql2 = "select * from miniproject where cid='$cid'";
			$result2 = $conn->query($sql2);
			if ($result2->num_rows > 0) {
				$i=1;
			while($row2 = $result2->fetch_assoc()){
					$pnm=$row2["pnm"];
					?>	
  <div class="w3-card-4" style="width:50%;">
    <header class="w3-container" style="background-color:#435e76"  >
      <h2 style="color:white" ><?php echo $pnm; ?></h2>
    </header>

    <div class="w3-container">
      <br/><p><?php echo $row2["prbdesc"]; ?></p>
    </div>

    <footer class="w3-container w3-center">
      <h4> <?php echo "<button class='btn btn-primary'><a href='miniprodetails.php?id=$pnm'>View</a></button>";?></h4>
    </footer>
  </div><br/><?php
				
			}
			}
		}
			?></div><?php
			echo "<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
		<script src='jquery.paginate.js'></script>
		<script>
		$(document).ready(function () {
		$('#search').paginate({
        'elemsPerPage': 1,
            'maxButtons': 6
    });
});
</script>";
		}
		
	}
}	


?>
  </div>
</div>
</div>



</body>
</html>

<?php include("footer.html"); ?>