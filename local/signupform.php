<?php 

	include("header.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sign Up</title>
  <link href="form.css" rel="stylesheet">
</head>
  
<body>

<div class="container">
<a href="home.php"><img class = "image" src="logod.png" alt="PHUB" height="50" width="50"></a>
 <div class="sgn">
    <form action="signupvld.php" method="post">
    <h4>SignUp for Projects hub.</h4>
  
    <h4><b>College Details</b></h4>
    <span>College Name:</span>
	<input type="text" class="inp" id="cname" name="cname" placeholder="College Name" required>

    <span>University: </span>
	<input type="text" class="inp" id="univ" name="univ" placeholder="University" required> 

	<span>Autonomous: </span>
	<input type="radio" id="atnms" name="atnms" value="Yes">Yes
	<input type="radio" id="atnms" name="atnms" value="No">No <br/><br/>
		
    <span>Address: </span>
	<input type="text" class="inp" id="cadd" name="cadd" placeholder="Address" required> 
    <span>City: </span>
	<input type="text" class="inp" id="ccity" name="ccity" placeholder="City" required> 
    <span>State: </span>
	<input type="text" class="inp" id="cstate" name="cstate" placeholder="State" required> 
	<span>Email:</span>
	<input type="email" class="inp" id="cem" name="cem" placeholder="Email" required> 
	<span>Phone:</span>
	<input type="number" class="inp" id="cphn" name="cphn" placeholder="Phone" required> <br/>
	
	<h4><b>Representative 1:</b></h4>
	<span>Name:</span>
	<input type="text" class="inp" id="cr1nm" name="cr1nm" placeholder="Name" required> 
	<span>Email:</span>
	<input type="email" class="inp" id="cr1em" name="cr1em" placeholder="Email" required> 
	<span>Phone:</span>
	<input type="number" class="inp" id="cr1phn" name="cr1phn" placeholder="Phone" required> <br/>
	
	<h4><b>Representative 2:</b></h4>
	<span>Name:</span>
	<input type="text" class="inp" id="cr2nm" name="cr2nm" placeholder="Name" required> 
	<span>Email:</span>
	<input type="email" class="inp" id="cr2em" name="cr2em" placeholder="Email" required> 
	<span>Phone:</span>
	<input type="number" class="inp" id="cr2phn" name="cr2phn" placeholder="Phone" required> <br/><br/>
		
	<input type="submit" value="Submit" id="submitnew" name="submitnew">
  </form>
  
</div>

</div>
</body>
</html>

<?php include("footer.html"); ?>
