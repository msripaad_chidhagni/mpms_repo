<?php
	session_start();
		include("header.php"); 
  //echo $_SESSION['login_user'];
  //echo $_SESSION['login_user_name'];
  if($_SESSION['login_user'] != 'College'){
	if($_SESSION['login_user'] == 'Admin'){
	  header('location:admdb.php');
	}
    else if($_SESSION['login_user'] == 'Student'){
	  header('location:stud.php');
	}
	else if($_SESSION['login_user'] == 'College Representative'){
	  header('location:collrep.php');
	}
	else{
  	  header('location:signinform.php');
	}
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <title>College</title>
  
  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link href="jquery.paginate.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="body.css">
<link rel="stylesheet" href="form.css">
<link rel="stylesheet" href="tables.css">

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">


<!-- Container (profile Section) -->

<div id="prof" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>PROFILE</h2>
      
<?php

include("dbconfig.php");
	$unm=$_SESSION['login_user_name'];
	
	$q = "select * from colleges where cem='$unm'";
	$result = $conn->query($q);
	$row = $result->fetch_assoc();
	$cid=$row["cid"];?>
	<div class="container">
	<div class="sgntb">
	
	<div id="tabled">
	<h4><b>College Details</b></h4>
	<?php
				echo '<form method="post">';
				echo "<table id='colleges'>";
				echo "<tr><th>College Name</th>";
				echo "<td>".$row['cname']."</td></tr>";
				
				echo "<tr><th>University</th>";
				echo "<td>".$row['univ']."</td></tr>";
				
				echo "<tr><th>Autonomous</th>";
				echo "<td>".$row['atnms']."</td></tr>";
				
				echo "<tr><th>Address</th>";
				echo "<td>".$row['cadd'].", ".$row['ccity'].", ".$row['cstate']."</td></tr>";
								
				echo "<tr><th>Email</th>";
				echo "<td>".$row['cem']."</td></tr>";
								
				echo "<tr><th>Phone</th>";
				echo "<td>".$row['cphn']."</td></tr>";
				
	echo "</table>";?><br/>
	
	
	<?php
	echo "<h4><b>College Representative 1:</b></h4>";
				echo "<table id='colleges'>";
				echo "<tr><th>Name</th>";
				echo "<td>".$row['cr1nm']."</td></tr>";
				
				echo "<tr><th>Email</th>";
				echo "<td>".$row['cr1em']."</td></tr>";
				
				echo "<tr><th>Phone</th>";
				echo "<td>".$row['cr1phn']."</td></tr>";
				
				echo "</table>";?><br/>
	<?php
	echo "<h4><b>College Representative 2:</b></h4>";
				echo "<table id='colleges'>";
				echo "<tr><th>Name</th>";
				echo "<td>".$row['cr2nm']."</td></tr>";
				
				echo "<tr><th>Email</th>";
				echo "<td>".$row['cr2em']."</td></tr>";
				
				echo "<tr><th>Phone</th>";
				echo "<td>".$row['cr2phn']."</td></tr>";
				
				echo "</table>";?><br/>
	
		</div>
		<button><a href="changepswform.php" align="right">Change Password</a></button>
	</div></h4>
	 <?php echo '</form>'; ?>
    </div>
	
	 
  </div>

  </div>
</div>
</div>


<!-- Container (dashboard Section) -->

<div id="db" class="container-fluid bg-grey">
  <div class="row">
    <div class="col-sm-8">
      <br/><h2>DASHBOARD</h2>
       
  
<!-- Team - Guide Container (dashboard Section) -->
<?php
$sql = "select distinct t.*,g.* from guide g,team t,colleges c where g.cid=c.cid and t.cid=c.cid and g.gid=t.gid";
$result = $conn->query($sql);?>
<div class="container">
 <div class="sgntb">
<?php echo '<form method="post" action="deletecoll.php">';?>
<h4><b>Team and Guide Details</b></h4>
<?php
if ($result->num_rows > 0) {
	?><div id="details"><?php
    echo "<table id='colleges'><thead><tr>
	<th>Team Name</th>
	<th>Active Status</th>
	<th>Guide Name</th>
	<th>Guide Designation</th>
	<th>Department</th>
	<th>Experience</th>
	<th>Specialization</th>
	<th>Email</th>
	<th>Phone</th>
	</tr></thead><tbody>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
		
		$tid=$row['tid'];
		$tnm=$row['tnm'];
		 echo "<tr><td><a href='teamdetails.php?id=$tid'>". $tnm."</a></td>";
        echo '</td><td>' .$row["tstatus"].'</td><td> '.$row["gnm"].'</td><td> '.$row["gdesg"]. 
			 '</td><td>' .$row["gdpt"]. '</td><td>' .$row["expyr"]. '</td><td>' .$row["spec"].
			 '</td><td>' .$row["gem"]. '</td><td>' .$row["gphn"]. '</td></tr>';
    }
	
    echo "</tbody>";
    echo "</table>";
	?></div><?php
			echo "<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
		<script src='jquery.paginate.js'></script>
		<script>
		$(document).ready(function () {
		$('#details').paginate({
        'elemsPerPage': 2,
            'maxButtons': 6
    });
});
</script>";
} else {
    echo "0 results";
}
echo '</form>';




echo '<script>
$("#checAl").click(function)() {
	$("input:checkbox").not(this).prop("checked",this.checked);
});
</script>';

?>   
  </div>
  
  </div>
  
 
  
<!-- Students Container (dashboard Section) -->
	
<?php


$sql = "select * from students where cid='$cid' and IsDeleted=0";
$result = $conn->query($sql);?>
<div class="container">
 <div class="sgntb">
<?php echo '<form method="post" action="deletecoll.php">';?>
<h4><b>Student Details</b></h4>
<?php
if ($result->num_rows > 0) {
	?><div id="studdetails"><?php
    echo "<table id='colleges'><thead><tr>
	<th>Roll Number</th>
	<th>Name</th>
	<th>Course</th>
	<th>Branch</th>
	<th>Section</th>
	<th>Email</th>
	<th>Phone</th>
	<th>Enrolled Year</th>
	<th>Active</th>
	<th>"; ?> <input type='submit' name='save' value='DELETE'> <?php echo "</th>
	</tr></thead><tbody>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
		$sroll=$row["sroll"];
        echo '<tr><td>' .$row["sroll"]. '</td><td>' .$row["sfnm"].' '.$row["smnm"].' '.$row["slnm"]. 
			 '</td><td>' .$row["course"]. '</td><td>' .$row["brnch"]. '</td><td>' .$row["ssec"].
			 '</td><td>' .$row["sem"]. '</td><td>' .$row["sphn"]. '</td><td>' .$row["senrl"].
			 '</td><td>' .$row["sstat"]. '</td>
			 <td><input type="checkbox" id="checkItem" name="check[]" value='.$sroll.'></td></tr>';
    }
	
    echo "</tbody>";
    echo "</table>";
	?></div><?php
		echo "<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
		<script src='jquery.paginate.js'></script>
		<script>
		$(document).ready(function () {
		$('#studdetails').paginate({
        'elemsPerPage': 2,
            'maxButtons': 6
    });
});
</script>";
} else {
    echo "0 results";
}
echo '</form>';




echo '<script>
$("#checAl").click(function)() {
	$("input:checkbox").not(this).prop("checked",this.checked);
});
</script>';

?>   
  </div>
  
  </div>
  


    </div>
  </div>
</div>



<!-- Container (adm add Section) -->

<div id="add" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>ADD STUDENT DETAILS</h2><br>

 
<div class="container">
 <div class="sgn">
 <h4><b>BULK UPLOAD</b></h4>

 <form action="addstudb.php" method="post" enctype="multipart/form-data"> 
 
    Select file to upload: 
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	
	<a href="sample_student_details_file.csv"><input type="button" value="Download Sample File"></a>

    <input type="file" name="studfile" id="studfile"> <br/>
    <input type="submit" value="Upload File" name="submits" id="submits"><br/>
</form>

</div>
</div>


<div class="container">
 <div class="sgn">
 <h4><b>MANUAL UPLOAD</b></h4>
  <form action="addstud.php" method="post">

	 
	<h4><b>Team Details</b></h4>
	<span>Team Name:</span>
	<input required type="text" class="inp" id="tnm" name="tnm" placeholder="Team Name"> 
	
	<span>Team Status:</span>
	<input required type="text" class="inp" id="tstatus" name="tstatus" placeholder="Team Status"> 
	
	<span>Date of Creation: </span>
	<input type="date" class="inp" id="tdoc" name="tdoc" placeholder="Date of Creation">
	<span>Date of Exit: </span>
	<input type="date" class="inp" id="tdoe" name="tdoe" placeholder="Date of Exit"><br/><br/><br/>
	
	

	
	
	<h4><b>Guide Details</b></h4>
    <span>Guide Name:</span>
	<input required type="text" class="inp" id="gnm" name="gnm" placeholder="Guide Name">
    <span>Designatoin:</span>
	<input required type="text" class="inp" id="gdesg" name="gdesg" placeholder="Designation">
    <span>Email:</span>
	<input required type="email" class="inp" id="gem" name="gem" placeholder="Email">
    <span>Phone:</span>
	<input required type="number" class="inp" id="gphn" name="gphn" placeholder="Phone">
    <span>Department:</span>
	<input required type="text" class="inp" id="gdpt" name="gdpt" placeholder="Department">
	<span>Experience:</span>
	<input required type="number" class="inp" id="expyr" name="expyr" placeholder="Experience(Years)">
	<span>Specialization:</span>
	<input required type="text" class="inp" id="spec" name="spec" placeholder="Specialization"><br/><br/>
	

	
	
	<h4><b>Student Details</b></h4>
    <span>First Name:</span>
	<input required type="text" class="inp" id="sfnm" name="sfnm" placeholder="First Name">
	<span>Middle Name:</span>
	<input required type="text" class="inp" id="smnm" name="smnm" placeholder="Middle Name">
	<span>Last Name:</span>
	<input required type="text" class="inp" id="slnm" name="slnm" placeholder="Last Name">

    <span>Roll Number: </span>
	<input required type="text" class="inp" id="sroll" name="sroll" placeholder="Roll Number"> 
	
	<span>Date of Birth: </span>
	<input required type="date" class="inp" id="sdob" name="sdob" placeholder="DOB">
	<span>Gender: </span>
	<input required type="text" class="inp" id="sgender" name="sgender" placeholder="Male/Female">
	<span>Year of Enrollment: </span>
	<input required type="number" class="inp" id="senrl" name="senrl" placeholder="YYYY">
	<span>Expected Year of Completion: </span>
	<input required type="number" class="inp" id="scmpl" name="scmpl" placeholder="YYYY">
	
	 <span>Branch: </span>
	<input required type="text" class="inp" id="brnch" name="brnch" placeholder="Branch">
	 <span>Course: </span>
	<input required type="text" class="inp" id="course" name="course" placeholder="Course">
	<span>Section: </span>
	<input required type="text" class="inp" id="ssec" name="ssec" placeholder="Section">
	
	 <span>Active Status: </span>
	<input required type="text" class="inp" id="sstat" name="sstat" placeholder="Yes/No">
	

	<span>Email:</span>
	<input required type="email" class="inp" id="sem" name="sem" placeholder="Email"> 
	<span>Phone:</span>
	<input required type="number" class="inp" id="sphn" name="sphn" placeholder="Phone"> 
	

	<span>Address:</span>
	<input required type="text" class="inp" id="sadd" name="sadd" placeholder="Address"> 
	<span>City:</span>
	<input required type="text" class="inp" id="scity" name="scity" placeholder="City"> 
	<span>State:</span>
	<input required type="text" class="inp" id="sstate" name="sstate" placeholder="State"> <br/>
	
	
	<div id="div_quotes1"> </div><br/>
	
	<input type="button" value="Add Student details" onclick="addstudent()"><br/><br/>
	
	<input type="submit" value="Submit">
	
  </form>
	
</div>

</div>
	
</div>

</div>
      
</div>


<!-- Container (modify Section) -->

<div id="modify" class="container-fluid bg-grey">
  <div class="row">
    <div class="col-sm-8">
	
      <h2>Modify College Details</h2><br>
	
<div class="container">
 <div class="sgn">


  <form action="modifycollege.php" method="post">
		   
  
	<h4><b>College Details</b></h4>
   
	<span>Autonomous: </span>
	<input type="radio" id="atnms" name="atnms" value="Yes">Yes
	<input type="radio" id="atnms" name="atnms" value="No">No <br/><br/>
		
	<span>Phone:</span>
	<input type="number" class="inp" id="cphn" name="cphn" placeholder="Phone" > <br/>
	
	<h4><b>Representative 1:</b></h4>
	<span>Name:</span>
	<input type="text" class="inp" id="cr1nm" name="cr1nm" placeholder="Name" > 
	<span>Email:</span>
	<input type="email" class="inp" id="cr1em" name="cr1em" placeholder="Email" > 
	<span>Phone:</span>
	<input type="number" class="inp" id="cr1phn" name="cr1phn" placeholder="Phone" > <br/>
	
	<h4><b>Representative 2:</b></h4>
	<span>Name:</span>
	<input type="text" class="inp" id="cr2nm" name="cr2nm" placeholder="Name" > 
	<span>Email:</span>
	<input type="email" class="inp" id="cr2em" name="cr2em" placeholder="Email" > 
	<span>Phone:</span>
	<input type="number" class="inp" id="cr2phn" name="cr2phn" placeholder="Phone" > <br/><br/>
	
		
	<input type="submit" value="Submit"  name="submitm" id="submitm">
  </form>
  
  </div>
  </div>
    </div>
  </div>
</div>




<script>


function addstudent(){
	
			
			//var k=document.getElementById("sid").value;
			var k2=document.getElementById("sfnm").value;
			var k3=document.getElementById("smnm").value;
			var k4=document.getElementById("slnm").value;
			var k5=document.getElementById("sroll").value;
			var k6=document.getElementById("sdob").value;
			//var k7=document.getElementById("sdoe").value;
			var k8=document.getElementById("sgender").value;
			var k9=document.getElementById("senrl").value;
			var k10=document.getElementById("scmpl").value;
			var k11=document.getElementById("brnch").value;
			var k12=document.getElementById("course").value;
			var k13=document.getElementById("ssec").value;
			var k14=document.getElementById("sstat").value;
			var k15=document.getElementById("sem").value;
			var k16=document.getElementById("sphn").value;
			var k17=document.getElementById("sadd").value;
			var k18=document.getElementById("scity").value;
			var k19=document.getElementById("sstate").value;
			
			var div = document.getElementById('div_quotes1');
			
          div.innerHTML += "Entered Student Details:<br/><br/>First Name:<input  class='inp' name='sfnm[]' value="+k2+" readonly></input>";
            div.innerHTML += "Middle  Name:<input class='inp' name='smnm[]' value="+k3+" readonly></input>";
            div.innerHTML += "Last  Name:<input  class='inp' name='slnm[]' value="+k4+" readonly></input>";
            div.innerHTML += "Roll No:<input  class='inp' name='sroll[]' value="+k5+" readonly></input><br/><br/>";
            div.innerHTML += "Date of Birth:<input  class='inp' name='sdob[]' value="+k6+" readonly></input>";
            div.innerHTML += "Gender:<input  class='inp' name='sgender[]' value="+k8+" readonly></input>";
            div.innerHTML += "Year of Enrolment:<input  class='inp' name='senrl[]' value="+k9+" readonly></input><br/><br/>";
            div.innerHTML += "Year of Completion:<input  class='inp' name='scmpl[]' value="+k10+" readonly></input><br/><br/>";
            div.innerHTML += "Branch:<input  class='inp' name='brnch[]' value="+k11+" readonly></input>";
            div.innerHTML += "Course:<input  class='inp' name='course[]' value="+k12+" readonly></input>";
            div.innerHTML += "Section:<input  class='inp' name='ssec[]' value="+k13+" readonly></input>";
            div.innerHTML += "Active Status:<input  class='inp' name='sstat[]' value="+k14+" readonly></input><br/><br/>";
            div.innerHTML += "Email:<input  class='inp' name='sem[]' value="+k15+" readonly></input>";
            div.innerHTML += "Phone:<input  class='inp' name='sphn[]' value="+k16+" readonly></input><br/><br/>";
            div.innerHTML += "Address:<input  class='inp' name='sadd[]' value="+k17+" readonly></input>";
            div.innerHTML += "City:<input  class='inp' name='scity[]' value="+k18+" readonly></input>";
            div.innerHTML += "State:<input  class='inp' name='sstate[]' value="+k19+" readonly></input> <br/><br/><br/>";
            div.innerHTML += "\n";
		
			
        }

		

		
</script>

</body>
</html>

<?php include("footer.html"); ?>