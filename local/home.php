<?php

		session_start();
		include("header.php"); 
?>

<!DOCTYPE html>
<html lang="en">
<head>
 <title>Projects Hub</title>

 <link href="full-slider.css" rel="stylesheet">


</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<div id="myCarousel" class="carousel slide">
    
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        
        <div class="carousel-inner">
            <div class="item active">
            
                <div class="fill" style="background-image:url('http://placehold.it/1900x1080&text=PROJECTS HUB');"></div>
                <div class="carousel-caption">
                    <h2>Projects at one place</h2>
                </div>
            </div>
            <div class="item">
               
                <div class="fill" style="background-image:url('http://placehold.it/1900x1080&text=Slide Two');"></div>
                <div class="carousel-caption">
                    <h2>Caption 2</h2>
                </div>
            </div>
            <div class="item">
               
                <div class="fill" style="background-image:url('http://placehold.it/1900x1080&text=Slide Three');"></div>
                <div class="carousel-caption">
                    <h2>Caption 3</h2>
                </div>
            </div>
        </div>

       
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </div>


<!--<div class="jumbotron text-center">
  <h1>PROJECTS HUB</h1> 
  <p>Projects at one place</p> 
  <form method="post" action="miniprosearch.php" class="form-group">
    <div class="input-group">
     <!-- <input type="text" class="form-control" size="50" placeholder="Search Text" required>
      <div class="input-group-btn">
        <button type="button" class="btn btn-danger">Search</button>
      </div>
	  <div class="col-md-6 col-md-offset-3">
		<input type="text" class="form-control" size="200" placeholder="Text input" name="searchele" >
		</div>
		<div class="col-md-2 ">
			<button type="submit" class="btn btn-danger" name="search">Search</button>
		</div>
    </div>
  </form>
</div>-->

<!-- Container (About Section) -->
<div id="about" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>About</h2><br>
      <h4>Projects Hub is a place where one get info about all
      projects of all engineering students, from different colleges.</h4><br>
      <p>Registered Colleges can add student, project, team, details.</p>
      <p>Registered Students can modify their particular projects.</p>
      
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-signal logo"></span>
    </div>
  </div>
</div>

<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
 <h2>Contact</h2><br>
  <div class="row">
    <div class="col-sm-5">
      <p>Contact us and we'll get back to you within 24 hours.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> Pragathi Nagar, Hderabad</p>
      <p><span class="glyphicon glyphicon-phone"></span> +91 9999999999</p>
      <p><span class="glyphicon glyphicon-envelope"></span> juggernauts@chidhagni.com </p>
    </div>
    

<!-- Add Google Maps -->
<div id="googleMap" style="height:300px;width:50%;" align=right></div>
<script>
function myMap() {
var myCenter = new google.maps.LatLng(17.5159904, 78.3960801);
var mapProp = {center:myCenter, zoom:12, scrollwheel:true, draggable:true, mapTypeId:google.maps.MapTypeId.ROADMAP};
var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
var marker = new google.maps.Marker({position:myCenter});
   var trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(map);
marker.setMap(map);
}
</script></div></div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAR0GTO_NC9UWAlHLuZwkICpKYW4JVRkdk&callback=myMap"></script>
<!--
To use this code on your website, get a free API key from Google.
Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
-->

<!--<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>
  <p>Visit our website <a href="http://www.chidhagni.com" title="Visit Chidhagni" target="_blank">www.chidhagni.com</a></p>
</footer>-->


</body>
</html>

<?php include("footer.html"); ?>


