-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for mpms
CREATE DATABASE IF NOT EXISTS `mpms` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mpms`;

-- Dumping structure for table mpms.colleges
CREATE TABLE IF NOT EXISTS `colleges` (
  `cid` varchar(50) NOT NULL,
  `cname` varchar(50) NOT NULL,
  `univ` varchar(50) NOT NULL,
  `atnms` varchar(50) NOT NULL,
  `cadd` varchar(50) NOT NULL,
  `ccity` varchar(50) NOT NULL,
  `cstate` varchar(50) NOT NULL,
  `cem` varchar(50) NOT NULL,
  `cphn` bigint(20) NOT NULL,
  `cr1nm` varchar(50) NOT NULL,
  `cr1em` varchar(50) NOT NULL,
  `cr1phn` bigint(20) NOT NULL,
  `cr2nm` varchar(50) NOT NULL,
  `cr2em` varchar(50) NOT NULL,
  `cr2phn` bigint(20) NOT NULL,
  `id` varchar(50) NOT NULL,
  `inc` bigint(20) NOT NULL AUTO_INCREMENT,
  `IsDeleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cname`,`cem`),
  UNIQUE KEY `cid` (`cid`),
  UNIQUE KEY `cr1em` (`cr1em`),
  UNIQUE KEY `cr1phn` (`cr1phn`),
  UNIQUE KEY `cr2phn` (`cr2phn`),
  UNIQUE KEY `cr2em` (`cr2em`),
  KEY `inc` (`inc`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.colleges: ~8 rows (approximately)
/*!40000 ALTER TABLE `colleges` DISABLE KEYS */;
INSERT INTO `colleges` (`cid`, `cname`, `univ`, `atnms`, `cadd`, `ccity`, `cstate`, `cem`, `cphn`, `cr1nm`, `cr1em`, `cr1phn`, `cr2nm`, `cr2em`, `cr2phn`, `id`, `inc`, `IsDeleted`) VALUES
	('C120180130224842', 'BVRIT', 'JNTUH', 'No', 'OU', 'Hyderabad', 'Telangana', 'bvr@it.in', 7485907476, 'Sripaada', 'abc@gml.com', 7475967497, 'Dheraj', 'adr.adr@gmail.cm', 7485967494, '', 1, 0),
	('C220180119121915', 'CBIT', 'OU', 'Yes', 'Gandipet', 'Hyderabad', 'Telangana', 'cbit@gmail.com', 7182937193, 'Areeb', 'areebuddin95@gmail.com', 7182937194, 'Dheeraj', 'dheeraj.thodupunoori01@gmail.com', 7182937195, '', 2, 0),
	('C20180123153818', 'CVR', 'JNTUH', 'No', 'CHAITANYAPURI', 'HYDERABAD', 'TELANGANA', 'cvr@c.com', 7852785202, 'Narayana', 'Narayana@a.a', 7852785203, 'ABC', 'abc@s.s', 7852785204, '', 3, 0),
	('C220180130224855', 'GNI', 'JNTUH', 'Yes', 'Gandipet', 'Hyderabad', 'Telangana', 'gni@gmail.com', 7102437193, 'Santhos', 'qwe@ty.com', 7102937194, 'Areb', 'avc@avc.avc', 7182937191, '', 4, 0),
	('C20180123110343', 'GRRR', 'JNTUH', 'No', 'SANTHOSH NAGAR', 'HYDERABAD', 'TELANGANA', 'grrr@gmail.com', 7845122154, 'Raj', 'abc@gmail.com', 7845122155, 'Rahul', 'abc12@gmail.com', 7845122156, '', 5, 2),
	('C120180119121903', 'OUCE', 'OU', 'No', 'OU', 'Hyderabad', 'Telangana', 'ou@ou.in', 4714714710, 'N M SRIPAAD', 'nmsripaad12@gmail.com', 9494351623, 'SANTOSH LAGISHETTY', 'santoshlagishetty@gmail.com', 9494351624, '', 8, 0),
	('C001', 'VCE', 'OU', 'Yes', 'Ibrahimbagh', 'Hyderabad', 'TS', 'vce@gmail.com', 1234567890, 'Sashi Kumar', 'sashi@gmail.com', 9494351622, 'Vinay Kumar', 'vinay@gmail.com', 9502148597, '', 9, 0),
	('C20180123153634', 'VNR', 'JNTUH', 'Yes', 'YADAGIRI NAGAR', 'HYDERABAD', 'TELANGANA', 'vnr@vjiet.com', 7894561280, 'Ramesh', 'ramesh@vnr.com', 7894561281, 'Ramu', 'ramu@vnr.com', 7894561282, '', 10, 2);
/*!40000 ALTER TABLE `colleges` ENABLE KEYS */;

-- Dumping structure for table mpms.guide
CREATE TABLE IF NOT EXISTS `guide` (
  `gid` varchar(50) NOT NULL,
  `gnm` varchar(50) NOT NULL,
  `gdesg` varchar(50) NOT NULL,
  `cid` varchar(50) NOT NULL,
  `gem` varchar(50) NOT NULL,
  `gphn` bigint(20) NOT NULL,
  `gdpt` varchar(50) NOT NULL,
  `expyr` varchar(50) NOT NULL,
  `spec` varchar(50) NOT NULL,
  PRIMARY KEY (`gem`),
  UNIQUE KEY `gid` (`gid`),
  UNIQUE KEY `gnm_gem` (`gnm`,`gem`),
  KEY `FK_guide_colleges` (`cid`),
  CONSTRAINT `FK_guide_colleges` FOREIGN KEY (`cid`) REFERENCES `colleges` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.guide: ~5 rows (approximately)
/*!40000 ALTER TABLE `guide` DISABLE KEYS */;
INSERT INTO `guide` (`gid`, `gnm`, `gdesg`, `cid`, `gem`, `gphn`, `gdpt`, `expyr`, `spec`) VALUES
	('G120180201211543', 'Vinay', 'Professor', 'C120180119121903', 'abc@a.abc', 4584584580, 'CSE', '16', 'ES'),
	('G20180128171626', 'Areeb', 'Professor', 'C120180119121903', 'abcareeb123@gmail.com', 741852963, 'CSE', '12', 'DM'),
	('G220180201211555', 'Vijay', 'Professor', 'C120180119121903', 'abcd@a.abcd', 4584584590, 'CSE', '17', 'ES'),
	('G20180128152332', 'Sashi', 'Professor', 'C120180119121903', 'ASD@ASD.A', 4545454545, 'CSE', '12', 'DM'),
	('G0001', 'Dr.Adi Lakshmi', 'HOD', 'C20180123153634', 'csehod@vce.ac.in', 1234567890, 'CSE', '30', 'DM');
/*!40000 ALTER TABLE `guide` ENABLE KEYS */;

-- Dumping structure for table mpms.miniproject
CREATE TABLE IF NOT EXISTS `miniproject` (
  `pid` varchar(50) NOT NULL,
  `pnm` varchar(50) NOT NULL,
  `pcat1` varchar(50) NOT NULL,
  `pcat2` varchar(50) DEFAULT NULL,
  `pdura` int(11) NOT NULL,
  `cid` varchar(50) NOT NULL,
  `pstatus` varchar(50) NOT NULL,
  `pstg` varchar(50) NOT NULL,
  `prbstmt` varchar(250) NOT NULL,
  `prbdesc` varchar(250) NOT NULL,
  `pvid` varchar(100) NOT NULL,
  `sf` varchar(150) NOT NULL,
  `ae` varchar(50) NOT NULL,
  `advs` varchar(50) NOT NULL,
  `gid` varchar(50) NOT NULL,
  `tid` varchar(50) NOT NULL,
  PRIMARY KEY (`pid`),
  KEY `FK_miniproject_colleges` (`cid`),
  KEY `FK_miniproject_guide` (`gid`),
  KEY `FK_miniproject_team` (`tid`),
  CONSTRAINT `FK_miniproject_colleges` FOREIGN KEY (`cid`) REFERENCES `colleges` (`cid`),
  CONSTRAINT `FK_miniproject_guide` FOREIGN KEY (`gid`) REFERENCES `guide` (`gid`),
  CONSTRAINT `FK_miniproject_team` FOREIGN KEY (`tid`) REFERENCES `team` (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.miniproject: ~4 rows (approximately)
/*!40000 ALTER TABLE `miniproject` DISABLE KEYS */;
INSERT INTO `miniproject` (`pid`, `pnm`, `pcat1`, `pcat2`, `pdura`, `cid`, `pstatus`, `pstg`, `prbstmt`, `prbdesc`, `pvid`, `sf`, `ae`, `advs`, `gid`, `tid`) VALUES
	('P00001', 'Mini Projects Management System', 'Website', 'App2', 1, 'C001', 'Yes', 'Final', 'Mini Projects Management System', 'Projects Hub is a place where one get info about all projects of all engineering students, from different colleges.', 'https://www.youtube.com/embed/MpRRckA2Bo8', 'https://github.com/tejasai97/MachineLearning_Algorithms', 'No', 'Advantages', 'G0001', 'T120180201211543'),
	('P00002', 'Mini', 'Web', 'App', 1, 'C001', 'Yes', 'Final', 'Mini', 'Mini', 'https://www.youtube.com/embed/MpRRckA2Bo8', 'https://github.com/tejasai97/MachineLearning_Algorithms', 'No', 'Advantages', 'G0001', 'T0001'),
	('P20180124125458', 'ABC', 'App', 'App2', 2, 'C120180119121903', 'Yes', 'Initial', 'PS', 'PD', '', 'https://github.com/tejasai97/MachineLearning_Algorithms', 'Yes', 'Advs', 'G0001', 'T0001'),
	('P20180125225232', 'Projects', 'Web', 'HTML', 1, 'C120180119121903', 'Yes', 'Final', 'PS', 'PD', 'https://www.youtube.com/embed/yfoY53QXEnI', 'https://github.com/tejasai97/MachineLearning_Algorithms', 'Yes', 'None', 'G0001', 'T0001');
/*!40000 ALTER TABLE `miniproject` ENABLE KEYS */;

-- Dumping structure for table mpms.sample
CREATE TABLE IF NOT EXISTS `sample` (
  `id` varchar(50) NOT NULL,
  `inc` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `uid` varchar(50) NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `inc` (`inc`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.sample: ~1 rows (approximately)
/*!40000 ALTER TABLE `sample` DISABLE KEYS */;
INSERT INTO `sample` (`id`, `inc`, `name`, `uid`) VALUES
	('S20180207150215', 4, 'raju', 'S201802071502154');
/*!40000 ALTER TABLE `sample` ENABLE KEYS */;

-- Dumping structure for table mpms.skillset
CREATE TABLE IF NOT EXISTS `skillset` (
  `sid` varchar(50) DEFAULT NULL,
  `techid` varchar(50) DEFAULT NULL,
  `sstatintech` varchar(50) DEFAULT NULL,
  `exptlvl` varchar(50) DEFAULT NULL,
  UNIQUE KEY `sid_techid` (`sid`,`techid`),
  KEY `FK_skillset_technologies` (`techid`),
  CONSTRAINT `FK_skillset_students` FOREIGN KEY (`sid`) REFERENCES `students` (`sid`),
  CONSTRAINT `FK_skillset_technologies` FOREIGN KEY (`techid`) REFERENCES `technologies` (`techid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.skillset: ~4 rows (approximately)
/*!40000 ALTER TABLE `skillset` DISABLE KEYS */;
INSERT INTO `skillset` (`sid`, `techid`, `sstatintech`, `exptlvl`) VALUES
	('S0001', 'TC001', 'Active', 'Intermediate'),
	('S0001', 'TC002', 'Active', 'Intermediate'),
	('S20180121200415', 'TCL120180124125458', 'Yes', 'Beginner'),
	('S20180121200415', 'TCL020180124154849', 'Yes', 'Advanced'),
	('S20180121200415', 'TC002', 'Yes', 'Advanced');
/*!40000 ALTER TABLE `skillset` ENABLE KEYS */;

-- Dumping structure for table mpms.students
CREATE TABLE IF NOT EXISTS `students` (
  `sid` varchar(50) NOT NULL,
  `sfnm` varchar(50) NOT NULL,
  `smnm` varchar(50) NOT NULL,
  `slnm` varchar(50) NOT NULL,
  `sroll` varchar(50) NOT NULL,
  `sdob` date NOT NULL,
  `sdoe` date NOT NULL,
  `sgender` varchar(50) NOT NULL,
  `senrl` year(4) NOT NULL,
  `scmpl` year(4) NOT NULL,
  `brnch` varchar(50) NOT NULL,
  `course` varchar(50) NOT NULL,
  `ssec` varchar(50) NOT NULL,
  `sstat` varchar(50) NOT NULL,
  `cid` varchar(50) NOT NULL,
  `sem` varchar(50) NOT NULL,
  `sphn` bigint(20) NOT NULL,
  `sadd` varchar(50) NOT NULL,
  `scity` varchar(50) NOT NULL,
  `sstate` varchar(50) NOT NULL,
  `IsDeleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sem`),
  UNIQUE KEY `sid` (`sid`),
  UNIQUE KEY `sroll` (`sroll`),
  KEY `FK_students_colleges` (`cid`),
  CONSTRAINT `FK_students_colleges` FOREIGN KEY (`cid`) REFERENCES `colleges` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.students: ~7 rows (approximately)
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` (`sid`, `sfnm`, `smnm`, `slnm`, `sroll`, `sdob`, `sdoe`, `sgender`, `senrl`, `scmpl`, `brnch`, `course`, `ssec`, `sstat`, `cid`, `sem`, `sphn`, `sadd`, `scity`, `sstate`, `IsDeleted`) VALUES
	('S0001', 'Manichandra', 'Sripaad', 'Nemmikanti', '160214733021', '1997-01-12', '2018-01-09', 'Male', '2014', '2018', 'CSE', 'BE', 'A', 'Yes', 'C001', 'abc@gmail.com', 1245367908, 'Santosh nagar', 'Hyd', 'TS', 0),
	('S120180128171746', 'Asd', 'P', 'P', '160214733007', '1995-05-24', '0000-00-00', 'Male', '2014', '2018', 'CSE', 'BE', 'A', 'Yes', 'C120180119121903', 'asdf@123.com', 4564564560, 'CHAITANYAPURI', 'HYDERABAD', 'Telangana', 0),
	('S20180121200415', 'MANICHANDRA', 'SRIPAAD', 'NEMMIKANTI', '160214733025', '1999-01-05', '2018-01-21', 'Male', '2015', '2019', 'CSE', 'BE', 'A', 'Yes', 'C120180119121903', 'nmsripaad12@outlook.com', 9494351622, 'YADAGIRI NAGAR', 'HYDERABAD', 'TELANGANA', 0),
	('S220180201211543', 'Raghu', 'Kumar', 'R', '1602-14-733-066', '1997-04-15', '2018-02-01', 'Male', '2015', '2019', 'CSE', 'BE', 'A', 'Yes', 'C120180119121903', 'raghuk123@g.c', 7512127213, 'Pragathi Nagar', 'Hyderabad', 'Telangana', 0),
	('S320180201211555', 'Rahul', 'Kumar', 'B', '1602-14-733-067', '1997-05-05', '2018-02-01', 'Male', '2015', '2019', 'CSE', 'BE', 'A', 'Yes', 'C120180119121903', 'rahulk123b@g.c', 7542121213, 'Pragathi Nagar', 'Hyderabad', 'Telangana', 0),
	('S120180121200651', 'Raj', 'Kumar', 'M', '160214733023', '0000-00-00', '2018-01-21', 'Male', '2015', '2019', 'CSE', 'BE', 'A', 'Yes', 'C120180119121903', 'raj@gmail.com', 7531599513, 'Pragathi Nagar', 'Hyderabad', 'Telangana', 0),
	('S120180201211543', 'Raju', 'Kumar', 'R', '1602-14-733-065', '1997-03-03', '2018-02-01', 'Male', '2015', '2019', 'CSE', 'BE', 'A', 'Yes', 'C120180119121903', 'rajuk123@g.c', 7512121213, 'Pragathi Nagar', 'Hyderabad', 'Telangana', 0),
	('S020180128171626', 'Yeshwanth', 'P', 'P', '160214733003', '1995-05-24', '0000-00-00', 'Male', '2014', '2018', 'CSE', 'BE', 'A', 'Yes', 'C120180119121903', 'yesh123@y.t', 4564564560, 'CHAITANYAPURI', 'HYDERABAD', 'Telangana', 0);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;

-- Dumping structure for table mpms.team
CREATE TABLE IF NOT EXISTS `team` (
  `tid` varchar(50) NOT NULL,
  `cid` varchar(50) NOT NULL,
  `tnm` varchar(50) NOT NULL,
  `gid` varchar(50) NOT NULL,
  `tstatus` varchar(50) NOT NULL,
  `tdoc` date NOT NULL,
  `tdoe` date DEFAULT NULL,
  PRIMARY KEY (`tid`),
  KEY `FK_team_colleges` (`cid`),
  KEY `FK_team_guide` (`gid`),
  CONSTRAINT `FK_team_colleges` FOREIGN KEY (`cid`) REFERENCES `colleges` (`cid`),
  CONSTRAINT `FK_team_guide` FOREIGN KEY (`gid`) REFERENCES `guide` (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.team: ~4 rows (approximately)
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` (`tid`, `cid`, `tnm`, `gid`, `tstatus`, `tdoc`, `tdoe`) VALUES
	('T0001', 'C120180119121903', 'Juggernauts', 'G20180128152332', 'Yes', '2018-01-09', '2018-06-30'),
	('T120180201211543', 'C120180119121903', 'Thinkers', 'G120180201211543', 'Yes', '2018-01-18', '0000-00-00'),
	('T20180128171626', 'C120180119121903', 'JTEAM', 'G20180128171626', 'Yes', '2018-01-09', '2018-01-16'),
	('T220180201211555', 'C120180119121903', 'Thinkerspace', 'G220180201211555', 'Yes', '2018-01-18', '0000-00-00');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;

-- Dumping structure for table mpms.teamstuds
CREATE TABLE IF NOT EXISTS `teamstuds` (
  `sid` varchar(50) NOT NULL,
  `tid` varchar(50) NOT NULL,
  `sstatintm` varchar(50) NOT NULL DEFAULT 'Yes',
  `sdoj` date NOT NULL,
  `sdoe` date DEFAULT NULL,
  KEY `FK_teamstuds_students` (`sid`),
  KEY `FK_teamstuds_team` (`tid`),
  CONSTRAINT `FK_teamstuds_students` FOREIGN KEY (`sid`) REFERENCES `students` (`sid`),
  CONSTRAINT `FK_teamstuds_team` FOREIGN KEY (`tid`) REFERENCES `team` (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.teamstuds: ~6 rows (approximately)
/*!40000 ALTER TABLE `teamstuds` DISABLE KEYS */;
INSERT INTO `teamstuds` (`sid`, `tid`, `sstatintm`, `sdoj`, `sdoe`) VALUES
	('S0001', 'T0001', 'Yes', '2018-01-09', NULL),
	('S20180121200415', 'T0001', 'Yes', '2018-01-04', '0000-00-00'),
	('S020180128171626', 'T20180128171626', 'Yes', '2018-01-09', '2018-01-28'),
	('S20180121200415', 'T120180201211543', 'Yes', '2018-02-01', '0000-00-00'),
	('S220180201211543', 'T120180201211543', 'Yes', '2018-02-01', '0000-00-00'),
	('S320180201211555', 'T220180201211555', 'Yes', '2018-02-01', '0000-00-00');
/*!40000 ALTER TABLE `teamstuds` ENABLE KEYS */;

-- Dumping structure for table mpms.technologies
CREATE TABLE IF NOT EXISTS `technologies` (
  `techid` varchar(50) NOT NULL,
  `technm` varchar(50) NOT NULL,
  `techcat` varchar(50) NOT NULL,
  PRIMARY KEY (`technm`),
  UNIQUE KEY `techid` (`techid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.technologies: ~11 rows (approximately)
/*!40000 ALTER TABLE `technologies` DISABLE KEYS */;
INSERT INTO `technologies` (`techid`, `technm`, `techcat`) VALUES
	('TC002', 'C++', 'Language'),
	('TCI020180124125458', 'Eclipse', 'IDE'),
	('TCO120180124125458', 'Git', 'Others'),
	('TC001', 'HTML', 'Web Technology'),
	('TCL120180124125458', 'Java', 'Language'),
	('TCW020180124125458', 'JS', 'Web Technology'),
	('TCD020180124125458', 'mySQL', 'Database'),
	('TCI120180124125458', 'Notepad', 'IDE'),
	('TCI020180125224938', 'Notepad++', 'IDE'),
	('TCO020180124125458', 'Other', 'Others'),
	('TCL020180124154849', 'R', 'Language'),
	('TCS020180124125458', 'Xampp', 'Server');
/*!40000 ALTER TABLE `technologies` ENABLE KEYS */;

-- Dumping structure for table mpms.techused
CREATE TABLE IF NOT EXISTS `techused` (
  `pid` varchar(50),
  `techid` varchar(50),
  `IsDeleted` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `pid_techid` (`pid`,`techid`),
  KEY `FK_techused_technologies` (`techid`),
  CONSTRAINT `FK_techused_miniproject` FOREIGN KEY (`pid`) REFERENCES `miniproject` (`pid`),
  CONSTRAINT `FK_techused_technologies` FOREIGN KEY (`techid`) REFERENCES `technologies` (`techid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.techused: ~11 rows (approximately)
/*!40000 ALTER TABLE `techused` DISABLE KEYS */;
INSERT INTO `techused` (`pid`, `techid`, `IsDeleted`) VALUES
	('P00001', 'TC002', 0),
	('P20180124125458', 'TC002', 1),
	('P20180124125458', 'TCL120180124125458', 1),
	('P20180124125458', 'TCI020180124125458', 0),
	('P20180124125458', 'TCI120180124125458', 0),
	('P20180124125458', 'TCW020180124125458', 1),
	('P20180124125458', 'TCS020180124125458', 0),
	('P20180124125458', 'TCD020180124125458', 0),
	('P20180124125458', 'TCO020180124125458', 0),
	('P20180124125458', 'TCO120180124125458', 0),
	('P00001', 'TC001', 1),
	('P20180125225232', 'TCI020180125224938', 0);
/*!40000 ALTER TABLE `techused` ENABLE KEYS */;

-- Dumping structure for table mpms.useridpsw
CREATE TABLE IF NOT EXISTS `useridpsw` (
  `uid` varchar(50) NOT NULL,
  `unm` varchar(50) NOT NULL,
  `pswd` varchar(50) NOT NULL,
  `utype` varchar(50) NOT NULL,
  PRIMARY KEY (`unm`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mpms.useridpsw: ~33 rows (approximately)
/*!40000 ALTER TABLE `useridpsw` DISABLE KEYS */;
INSERT INTO `useridpsw` (`uid`, `unm`, `pswd`, `utype`) VALUES
	('CR220180123150828', 'abc12@gmail.com', 'changeme123', 'College Representative'),
	('CR120180123150828', 'abc@gmail.com', 'changeme123', 'College Representative'),
	('CR120180206105825', 'abc@gmail.comqwerty', 'changeme123', 'College Representative'),
	('CR1120180130224842', 'abc@gml.com', 'changeme123', 'College Representative'),
	('CR220180125203837', 'abc@s.s', 'changeme123', 'College Representative'),
	('CR220180206105825', 'abc@s.ssc', 'changeme123', 'College Representative'),
	('CR2120180130224842', 'adr.adr@gmail.cm', 'changeme123', 'College Representative'),
	('CR1220180119121915', 'areebuddin95@gmail.com', 'changeme123', 'College Representative'),
	('CR2220180130224855', 'avc@avc.avc', 'changeme123', 'College Representative'),
	('C20180125203837', 'cvr@c.com', 'changeme123', 'College'),
	('CR2120180119121903', 'dheeraj.thodupunoori01@gmail.com', 'changeme123', 'College Representative'),
	('C20180123150828', 'grrr@gmail.com', 'changeme123', 'College'),
	('A01', 'msripaad.nemmikanti@chidhagni.com', '12345', 'Admin'),
	('CR120180125203837', 'Narayana@a.a', 'changeme123', 'College Representative'),
	('CR120180202130320', 'naresh@ncit.c', 'changeme123', 'College Representative'),
	('C20180202130320', 'ncit@narayana.c', 'changeme123', 'College'),
	('C20180206105825', 'nms12@nms.com', 'changeme123', 'College'),
	('CR120180123125001', 'nmsripaad12@gmail.com', '12345', 'College Representative'),
	('S20180121200415', 'nmsripaad12@outlook.com', '12345', 'Student'),
	('C20180123125001', 'ou@ou.in', '12345', 'College'),
	('CR1220180130224855', 'qwe@ty.com', 'changeme123', 'College Representative'),
	('S220180201211543', 'raghuk123@g.c', 'changeme123', 'Student'),
	('S320180201211555', 'rahulk123b@g.c', 'changeme123', 'Student'),
	('S120180201211543', 'rajuk123@g.c', 'changeme123', 'Student'),
	('CR220180202130320', 'ramesh@ncit.c', 'changeme123', 'College Representative'),
	('CR120180201114643', 'ramesh@vnr.com', 'changeme123', 'College Representative'),
	('CR220180201114643', 'ramu@vnr.com', 'changeme123', 'College Representative'),
	('CR220180123125001', 'santoshlagishetty@gmail.com', '12345', 'College Representative'),
	('C001', 'vce@gmail.com', '12345', 'College'),
	('C20180201114643', 'vnr@vjiet.com', 'changeme123', 'College'),
	('S020180128171626', 'yesh123@y.t', 'changeme123', 'Student');
/*!40000 ALTER TABLE `useridpsw` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
