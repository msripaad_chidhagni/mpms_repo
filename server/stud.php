<?php

	session_start();
		include("header.php"); 
		
  //echo $_SESSION['login_user'];
 // echo $_SESSION['login_user_name'];
  if($_SESSION['login_user'] != 'Student'){
	if($_SESSION['login_user'] == 'Admin'){
	  header('location:admdb.php');
	}
    else if($_SESSION['login_user'] == 'College Representative'){
	  header('location:collrep.php');
	}
    else if($_SESSION['login_user'] == 'College'){
	  header('location:college.php');
	}
	else{
  	  header('location:signinform.php');
	}
  }
  

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Student</title>

<link rel="stylesheet" href="body.css">
<link rel="stylesheet" href="form.css">
<link rel="stylesheet" href="tables.css">
   
   		<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="jquery.easyPaginate.js"></script>

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">


<!-- Container (profile Section) -->

<div id="prof" class="container-fluid bg-grey">
  <div class="row">
    <div class="col-sm-8">
      <h2>Student Details</h2>
	<div class="container">
	<div class="sgntb">

<?php
include("dbconfig.php");
$unm=$_SESSION['login_user_name'];


	$q1 = "select * from students where sem='$unm'";
	$result1 = $conn->query($q1);
	$row1 = $result1->fetch_assoc();
	
	$cid=$row1["cid"];
	$sid=$row1["sid"];
	

	$q2 = "select * from colleges where cid='$cid'";
	$result2 = $conn->query($q2);
	$row2 = $result2->fetch_assoc();
	
	?>
	<h3>Personal Details:</h3><br/><?php

	echo "<table id='colleges'>";	
				echo "<th>Name</th>";
				 echo "<td>". $row1['sfnm']." ".$row1['smnm']." ".$row1['slnm']."</td></tr>";
				 
				  echo "<th>Date of Birth</th>";
				 echo "<td>". $row1['sdob']."</td></tr>";
				 
				 echo "<th>Email</th>";
				 echo "<td>". $row1['sem']."</td></tr>";
				 
				  echo "<th>Phone</th>";
				 echo "<td>". $row1['sphn']."</td></tr>";
				 
				  echo "<th>Address</th>";
				 echo "<td>". $row1['sadd']." ". $row1['scity']." ". $row1['sstate']."</td></tr>";
				 
				 
				  echo "<th>Active Status</th>";
				 echo "<td>". $row1['sstat']."</td></tr>";
				
				 echo "</table>";?>	

<h3>College Details:</h3><br/><?php

	echo "<table id='colleges'>";	
				echo "<th>College Name</th>";
				 echo "<td>". $row2['cname']."</td></tr>";
				 
				  echo "<th>University</th>";
				 echo "<td>". $row2['univ']."</td></tr>";
				 
				 echo "<th>Course</th>";
				 echo "<td>". $row1['course']."</td></tr>";
				 
				  echo "<th>Branch</th>";
				 echo "<td>". $row1['brnch']."</td></tr>";
				 
				  echo "<th>Section</th>";
				 echo "<td>". $row1['ssec']."</td></tr>";
				 
				  echo "<th>Roll Number</th>";
				 echo "<td>". $row1['sroll']."</td></tr>";
				 
				  echo "<th>Enrollment Year</th>";
				 echo "<td>". $row1['senrl']."</td></tr>";
				 
				  echo "<th>Completion Year</th>";
				 echo "<td>". $row1['scmpl']."</td></tr>";
				
				 echo "</table>";?>					 
	
	 <h3>Skills:</h3><br/>
	 <?php
		$q3="select * from skillset where sid='$sid'";
		$result3 = $conn->query($q3);
	
	
	if ($result3->num_rows > 0) {
    echo "<table id='colleges'><tr>
	<th>Technology</th>
	<th>Category</th>
	<th>Active Status</th>
	<th>Expert Level</th>
	</tr>";
    // output data of each row
    while($row3 = $result3->fetch_assoc()) {
		$techid=$row3["techid"];
		$q4="select * from technologies where techid='$techid'";
		$result4 = $conn->query($q4);
		$row4 = $result4->fetch_assoc();
		
        echo '<tr><td>' .$row4["technm"]. '</td><td>' .$row4["techcat"]. '</td>
			<td>' .$row3["sstatintech"]. '</td><td>' .$row3["exptlvl"]. '</td></tr>';
    }
    echo "</table>";
	}
	
 else {
    echo "0 results";
}
	 ?>
	
	 
	 
	<br/><div align="center" > <button><a href="prsnlform.php">Personalize</a></button> 
	<button><a href="changepswform.php">Change Password</a></button></div></h4>
	
    </div>
    </div>
    </div>
  
  </div>
</div>



<!-- Container (dashboard Section) -->

<div id="db" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>Projects Details</h2><br>
  <div class="container">
 <div class="sgntb">
<?php

// Create connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sem=$_SESSION['login_user_name'];
$sql = "select * from students where sem='$sem'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$sid=$row["sid"];

$sql2 = "select * from teamstuds where sid='$sid'";
$result2 = $conn->query($sql2);
$row2 = $result2->fetch_assoc();

$tid=$row2["tid"];



$sql1 = "select * from miniproject where tid='$tid'";
$result1 = $conn->query($sql1);

if ($result1->num_rows > 0) {
	echo "<div id='easyPaginate'>";
	$i=1;
	while($row1= $result1->fetch_assoc()) {
		$pid=$row1["pid"];
		$tid=$row1["tid"];
				 
			$sql2 = "select * from team where tid='$tid'";
			$result2 = $conn->query($sql2);
			$row2= $result2->fetch_assoc();
			
			
			
		
			
?><h4 style="color:435e76;">
<?php
				echo "<table id='colleges'>";
				 echo "<tr><th>Name</th>";
				 echo "<td>".$row1['pnm']."</td></tr>";
				 
				 echo "<tr><th>Category 1</th>";
				 echo "<td>".$row1['pcat1']."</td></tr>";
				 
				 echo "<tr><th>Category 2</th>";
				 echo "<td>".$row1['pcat2']."</td></tr>";
				 
				 echo "<tr><th>Duration</th>";
				 echo "<td>".$row1['pdura']." Month(s)</td></tr>";
				 	 
				 echo "<tr><th>Project Status</th>";
				 echo "<td>".$row1['pstatus']."</td></tr>";
				 
				 echo "<tr><th>Project Stage</th>";
				 echo "<td>".$row1['pstg']."</td></tr>";
				
				 echo "<tr><th>Problem Statement</th>";
				 echo "<td>".$row1['prbstmt']."</td></tr>";
				
				 echo "<tr><th>Problem Description</th>";
				 echo "<td>".$row1['prbdesc']."</td></tr>";
				 
				 echo "<tr><th>Already Exists</th>";
				 echo "<td>".$row1['ae']."</td></tr>";
				 
				 echo "<tr><th>Advantages</th>";
				 echo "<td>".$row1['advs']."</td></tr>";
				 
				  echo "<tr><th>Team Name</th>";
				 echo "<td>".$row2['tnm']."</td></tr>";
	
				 
				 echo "<tr><th>Video</th>" ;
				if($row1['pvid']!=null){
				echo '<td><iframe width="560" height="315" src="'.$row1['pvid'].'" frameborder="0" 
						allowfullscreen></iframe></td></tr>'; 
				}
				else{
				echo '<td>VIDEO LINK NOT AVIALABLE</td></tr>';
				}
	
	
				echo "<tr><th>Source File</th>" ;  
				if($row1['sf']!=null){
				echo '<td><a href= "'.$row1['sf'].'"; target="_blank">Github - Project Link</a>
				</td></tr>'; 
				}
				else{
				echo '<td>GITHUB LINK NOT AVIALABLE</td></tr>';
				}	

				echo "</table><br/><br/>";
				
		$q3="select * from techused where pid='$pid' and IsDeleted='0'";
		$result3 = $conn->query($q3);
	echo "<table id='colleges'><tr>
	<th>Technology</th>
	<th>Category</th></th>
	</tr>";
	
	if ($result3->num_rows > 0) {
    
    // output data of each row
    while($row3 = $result3->fetch_assoc()) {
		$techid=$row3["techid"];
		$q4="select * from technologies where techid='$techid'";
		$result4 = $conn->query($q4);
		$row4 = $result4->fetch_assoc();
		
        echo '<tr><td>' .$row4["technm"]. '</td><td>' .$row4["techcat"]. '</td></tr>';
    }
    echo "</table>";
	}
	
 else {
    echo "<td>NO TECHNOLOGIES AVIALABLE</td></table>";

echo '<td> </td>';
echo '<td> </td></tr></table>';
	}
 
	$pid=$row1["pid"];
  echo "<br/><button><a href='modifypform.php?id=$pid'>".'Modify Project Details'."</a></button></h4>"."\n";
	$i=$i+1;
}
echo "</div>";
echo "<script>
$('#easyPaginate').easyPaginate({
	paginateElement: 'h4',
	elementsPerPage: 1,
	effect: 'climb'
});
			</script>";
}

?>   
<script>
$('#easyPaginate').easyPaginate({
	paginateElement: 'h4',
	elementsPerPage: 12,
	effect: 'climb'
});
			</script>


 
  </div>
  </div>
  
  <h2>Team Details</h2><br>
  <div class="container">
 <div class="sgntb">
<?php

// Create connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sem=$_SESSION['login_user_name'];
$sql = "select * from students where sem='$sem'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$sid=$row["sid"];


$sql1 = "select * from teamstuds where sid='$sid'";
$result1 = $conn->query($sql1);

if ($result1->num_rows > 0) {
	
	while($row1= $result1->fetch_assoc()) {
		$tid=$row1["tid"];
		
				 
			$sql2 = "select * from team where tid='$tid'";
			$result2 = $conn->query($sql2);
			$row2= $result2->fetch_assoc();
			
?><h4 style="color:435e76;">
<?php
		echo "<table id='colleges'>";	
				echo "<th>Team Name</th>";
				 echo "<td>". $row2['tnm']."</td></tr>";
				 
				  echo "<th>Active in Team</th>";
				 echo "<td>". $row1['sstatintm']."</td></tr>";
				 
				 echo "<th>Date joined in Team</th>";
				 echo "<td>". $row1['sdoj']."</td></tr>";
				 
				 
				
				 echo "</table>";
	}
}

$conn->close();?>


 
  </div>
  </div>
  
  
  
  
  </div>
</div>
</div>



<!-- Container (adm add Section) -->

<div id="add" class="container-fluid bg-grey">
  <div class="row">
    <div class="col-sm-8">
      <h2>ADD PROJECT DETAILS</h2><br>
	<div class="container">
 <div class="sgn">
  <form action="addproj.php" method="post" enctype="multipart/form-data">
  
  
	<h4><b>Project Details</b></h4>
    <span>Project Name:</span>
	<input required type="text" class="inp" id="pnm" name="pnm" placeholder="Project Name" >
	<span>Category 1:</span>
	<input required type="text" class="inp" id="pcat1" name="pcat1" placeholder="Category 1">
	<span>Category 1:</span>
	<input required type="text" class="inp" id="pcat2" name="pcat2" placeholder="Category 2">
	
    <span>Project Duartion: </span>
	<input required type="number" class="inp" id="pdura" name="pdura" placeholder="Duration(Months)"> 
	
	<span>Project Active Status: </span>
	<input required type="text" class="inp" id="pstatus" name="pstatus" placeholder="Yes/No">
	<span>Project Stage: </span>
	<input required type="text" class="inp" id="pstg" name="pstg" placeholder="Initial/Middle/Final">
	
	
	
	<span>Problem Statement: </span><br/>
	<textarea required rows="4" cols="50" class="inp" id="prbstmt" name="prbstmt" placeholder="Problem Statement"></textarea>

	
	<span>Problem Description: </span><br/>
	<textarea required rows="4" cols="50" class="inp" id="prbdesc" name="prbdesc" placeholder="Problem Description"></textarea>

<!--Video file to upload:     <input type="file" name="pvid" id="pvid"> <br/>
	Source file to upload:     <input type="file" name="sf" id="sf"> <br/>
	-->
	
	
	<span>Video Link: </span> 
	<input required type="text" class="inp" id="pvid" name="pvid" placeholder="YouTube Video Embed Link">

	<span>Git Hub Link: </span>
	<input required type="text" class="inp" id="sf" name="sf" placeholder="Github Project Link">
	
	<span>Already Exists: </span>
	<input required type="text" class="inp" id="ae" name="ae" placeholder="Yes/No">
	<span>Advantages: </span>
	<textarea required rows="4" class="inp" id="advs" name="advs" placeholder="Advantages"></textarea><br/><br/>
	
	
	<h4><b>Technologies Used</b></h4>
   
   <span><h4>Languages:</h4></span>
	<input type="text" class="inp" id="lang" placeholder="Enter Language"/>
	<div id="div_quotes1"> </div><br/>
	<input type="button" value="Add" onclick="addlang()"><br/><br/>
	
	<span><h4>IDEs:</h4></span>
	<input required type="text" class="inp" id="ide" placeholder="Enter IDE">
	<div id="div_quotes2"> </div><br/>
	<input type="button" value="Add" onclick="addide()"><br/><br/>
	
	 
	<span><h4>Web Technologies:</h4></span>
	<input type="text" class="inp" id="wt" placeholder="Enter Web Technology">
	<div id="div_quotes3"> </div><br/>
	<input type="button" value="Add" onclick="addwt()"><br/><br/>
	
	<span><h4>Servers:</h4></span>
	<input type="text" class="inp" id="serv" placeholder="Enter Server">
	<div id="div_quotes4"> </div><br/>
	<input type="button" value="Add" onclick="addserv()"><br/><br/>
	
	 
	<span><h4>Databases:</h4></span>
	<input type="text" class="inp" id="dtb" placeholder="Enter Database">
	<div id="div_quotes5"> </div><br/>
	<input type="button" value="Add" onclick="adddtb()"><br/><br/>
	
	<span><h4>Others:</h4></span>
	<input type="text" class="inp" id="other" placeholder="Enter">
	<div id="div_quotes6"> </div><br/>
	<input type="button" value="Add" onclick="addother()"><br/><br/>
	
	 
	 
	 
	<input type="submit" value="Submit" name="submitp" id="submitp">
	
  </form>
	
</div>
</div>

</div>
      
    </div>
  
  </div>


<script>

function addlang(){
			var k=document.getElementById("lang").value;
			document.getElementById("lang").value = " ";
            var div = document.getElementById('div_quotes1');
            div.innerHTML += "<input class='inp'  name='lang[]' value="+k+" readonly></input>";
            div.innerHTML += "\n";
        }

function addide(){
    var k=document.getElementById("ide").value;
			document.getElementById("ide").value = " ";
            var div = document.getElementById('div_quotes2');
            div.innerHTML += "<input class='inp'  name='ide[]' value="+k+" readonly></input>";
            div.innerHTML += "\n";
}

function addwt(){
    var k=document.getElementById("wt").value;
			document.getElementById("wt").value = " ";
            var div = document.getElementById('div_quotes3');
            div.innerHTML += "<input class='inp'  name='wt[]' value="+k+" readonly></input>";
            div.innerHTML += "\n";
}

function addserv(){
   var k=document.getElementById("serv").value;
			document.getElementById("serv").value = " ";
            var div = document.getElementById('div_quotes4');
            div.innerHTML += "<input class='inp'  name='serv[]' value="+k+" readonly></input>";
            div.innerHTML += "\n";
}  

function adddtb(){
   var k=document.getElementById("dtb").value;
			document.getElementById("dtb").value = " ";
            var div = document.getElementById('div_quotes5');
            div.innerHTML += "<input class='inp'  name='dtb[]' value="+k+" readonly></input>";
            div.innerHTML += "\n";
}  

function addother(){
   var k=document.getElementById("other").value;
			document.getElementById("other").value = " ";
            var div = document.getElementById('div_quotes6');
            div.innerHTML += "<input class='inp'  name='other[]' value="+k+" readonly></input>";
            div.innerHTML += "\n";
}



</script>

</body>
</html>
<?php include("footer.html"); ?>