<?php
include("header.php");
?>
<?php
 session_start();
 if(isset($_SESSION['login_user'])){
 	if($_SESSION['login_user'] == 'Admin'){	 
			header('location:admdb.php');
		}
		else if($_SESSION['login_user'] == 'College Representative'){
			header('location:collrep.php');
		}
		else if($_SESSION['login_user'] == 'Student'){
			header('location:stud.php');
		}
		else if($_SESSION['login_user'] == 'College'){
			header('location:college.php');
		}
 }
 
 
 
  ?>
  
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sign In</title>
	<link href="form.css" rel="stylesheet">
</head>

<body>

<div class="container">
<a href="home.php"><img class="image" src="logod.png" alt="PHUB" height="50" width="50"></a>
 <div class="sgnin">
  <form action="signinvld.php" method="post">
  
	<h4>SignIn to Projects hub.</h4>
	
   	<span>Email:</span>
    <input type="email" class="inp" id="email" name="email" placeholder="Email" required>

 	<span>Password:</span>
    <input type="password" class="inp" id="password" name="password" placeholder="Password" required>
	
	<span><a href="forgot.php">Forgot?</a></span>
    
	<input type="submit" value="Submit" id="submitsignin" name="submitsignin">
  </form>
</div>
<br/>
</div>

</body>
</html>
<?php include("footer.html"); ?>