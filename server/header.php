<!DOCTYPE html>
<html lang="en">
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  	<!--<link rel="stylesheet" href="main.css">-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="body.css">
  <script src="links.js">
</script>

</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="home.php"><img src="logow.png" alt="PHUB" height="21" width="38"></a>
	  <a class="navbar-brand" href="home.php">PROJECTS HUB</a>
    </div>
	
    <div class="collapse navbar-collapse" id="myNavbar">
	
	<form class="navbar-form navbar-left" action="miniprosearch.php" method="post">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for Projects" name="searchele"/>
				<div class="input-group-btn">
					<button class="btn btn-default" type="submit" name="search">
						<i class="glyphicon glyphicon-search"></i>
					</button> 
					<!--<button type="submit" class="btn btn-danger" name="search">Search</button> -->
				</div>
		</div>
	</form>
	
      <!--<ul class="nav navbar-nav nav-right">
        <li><a href="#about">ABOUT</a></li>
        <li><a href="#contact">CONTACT</a></li>
		</ul>-->

		           
	<?php

		//session_start();
		if(isset($_SESSION['login_user'])){
			if($_SESSION['login_user'] == 'Admin'){?>
			
				 <ul class="nav navbar-nav navbar-right">
				<li><a href="#db" onclick="openWinAdmdb()">DASHBOARD</a></li>
				<li><a href="#add" onclick="openWinAdmadd()">ADD</a></li>
				<li><a href="signout.php">SIGN OUT</a></li>
       			</ul>
				
				<?php
			}
			else if($_SESSION['login_user'] == 'College Representative'){?>
				
				<ul class="nav navbar-nav navbar-right">
				<li><a href="#prof" onclick="openWinCRprof()">PROFILE</a></li>
				<li><a href="#db" onclick="openWinCRdb()">DASHBOARD</a></li>
				<li><a href="#add" onclick="openWinCRadd()">ADD</a></li>
				<li><a href="signout.php">SIGN OUT</a></li>
       			</ul>
				
				<?php
			}
			else if($_SESSION['login_user'] == 'Student'){?>
				
				<ul class="nav navbar-nav navbar-right">				
				<li><a href="#prof" onclick="openWinStudprof()">PROFILE</a></li>
				<li><a href="#db" onclick="openWinStudDB()">DASHBOARD</a></li>
				<li><a href="#add" onclick="openWinStudadd()">ADD</a></li>
				<li><a href="signout.php">SIGN OUT</a></li>
       			</ul>
			
			<?php	
			}
			else if($_SESSION['login_user'] == 'College'){?>
				
				<ul class="nav navbar-nav navbar-right">
				<li><a href="#prof" onclick="openWinCollegeprof()">PROFILE</a></li>
				<li><a href="#db" onclick="openWinCollegeDB()">DASHBOARD</a></li>
				<li><a href="#add" onclick="openWinCollegeAdd()">ADD</a></li>
				<li><a href="#modify" onclick="openWinCollegeModify()">MODIFY</a></li>
				<li><a href="signout.php">SIGN OUT</a></li>
       			</ul>
			
			<?php	
			}
		}
		else{?>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#about" onclick="openWinHomeAbout()">ABOUT</a></li>
				<li><a href="#contact" onclick="openWinHomeContact()">CONTACT</a></li>
				<li><a href="signupform.php">SIGN UP</a></li>
				<li><a href="signinform.php">SIGN IN</a></li>
       
			</ul>
			<?php
		}
	?>
	   
      
    </div>
  </div>
</nav>
</body>
</html>